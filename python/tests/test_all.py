#!/usr/bin/env tdaq_python
# $Id$
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 26-Mar-2007

# This python file runs most of the tests we need for the EventApps
# python bindings. It combines read/write-to-file tests, writing
# API exercises and, finally a whole combined test, with reading
# and writing.

from test_myopt import *

if __name__ == '__main__':
  import sys
  import unittest
  sys.argv.append('-v')
  unittest.main()
  
