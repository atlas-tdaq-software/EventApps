#!/usr/bin/env tdaq_python
# vim: set fileencoding=utf-8 :
# Created by Andre Anjos <andre.dos.anjos@cern.ch>
# Wed 27 Jan 2010 11:30:53 AM CET 

"""A test toy-program for the option parser.
"""

from EventApps.myopt import Parser

p = Parser(extra_args=True)

p.add_option(short='o', long='option', arg=True, description='', default=0)
p.add_option(short='e', long='expert', arg=True, description='', default=0,
    expert=True)

from sys import argv
opt, args = p.parse(argv[1:])

print(args)
