#!/usr/bin/env tdaq_python
#Andre dos Anjos <andre.dos.anjos@cern.ch>

"""Unit test for the Python bindings to EventApps."""

import unittest
import EventApps.myopt as myopt

class MyoptParser(unittest.TestCase):
  """Run multiple tests on our improved option parser."""
  
  def test01_canParseShort(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse('-o 4'.split())
    self.assertEqual(result['option'], 4)

  def test02_canParseLong(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse('--option 4'.split())
    self.assertEqual(result['option'], 4)

  def test03_canParseFloats(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse('--option 0.4'.split())
    self.assertEqual(result['option'], 0.4)
    
  def test04_canParseStrings(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse('--option "0.4"'.split())
    self.assertEqual(result['option'], "0.4")

  def test05_canParseLists(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse(['--option', '[1, 2, 3]'])
    self.assertEqual(result['option'], [1, 2, 3])
    
  def test06_canParseTuples(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse(['--option', '(1, 2, 3)'])
    self.assertEqual(result['option'], (1, 2, 3))
    
  def test07_canParseDictionaries(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse(['--option', '{"a": 1, "b": 2, "c": 3}'])
    self.assertEqual(result['option'], {'a':1,'b':2,'c':3})
    
  def test08_canParseComplicatedStuff(self):
    toparse = {"a":1, "b":(1,2,3,4), "c":[1, 2, 3, (42,), {"x":20, "y":21}]}
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse(['--option', str(toparse)])
    self.assertEqual(result['option'], toparse)

  def test09_canOverwrite(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse(['--option', '22', '--option=23'])
    self.assertEqual(result['option'], 23)

  def test10_canAddComponentsToLists(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=[])
    result = p.parse(['--option', '1'])
    self.assertEqual(result['option'], [1]) 
    result = p.parse(['--option', '2'])
    self.assertEqual(result['option'], [1, 2]) 
    result = p.parse(['--option', '3'])
    self.assertEqual(result['option'], [1, 2, 3]) 
    
  def test11_canResetOptionToBecomeAList(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    result = p.parse(['--option', '[]'])
    self.assertEqual(result['option'], []) 
    result = p.parse(['--option', '1'])
    self.assertEqual(result['option'], [1]) 
    result = p.parse(['--option', '2'])
    self.assertEqual(result['option'], [1, 2]) 
    result = p.parse(['--option', '3'])
    self.assertEqual(result['option'], [1, 2, 3]) 
    
  def test12_canAddToListsInComplicatedWay(self):
    p = myopt.Parser(extra_args=False)
    p.add_option(short='o', long='option', arg=True, description='', default=[])
    thing1 = (1, 2, 3)
    thing2 = (3, 4, 5)
    thing3 = {'a':1, 'b': 25} 
    result = p.parse(['--option', str(thing1)])
    self.assertEqual(result['option'], [thing1]) 
    result = p.parse(['--option', str(thing2)])
    self.assertEqual(result['option'], [thing1, thing2]) 
    result = p.parse(['--option', str(thing3)])
    self.assertEqual(result['option'], [thing1, thing2, thing3]) 
    
  def test13_canHaveExtraArguments(self):
    p = myopt.Parser(extra_args=True)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    thing = ['bla1', 'bla2', 'bla3']
    result, extra = p.parse(['--option=3'] + thing)
    self.assertEqual(result['option'], 3) 
    self.assertEqual(extra, thing)
    
  def test13_canHaveExtraArgumentsUnordered(self):
    p = myopt.Parser(extra_args=True)
    p.add_option(short='o', long='option', arg=True, description='', default=0)
    thing = ['bla1', 'bla2', 'bla3', '-o4']
    result, extra = p.parse(['--option=3'] + thing)
    self.assertEqual(result['option'], 4) 
    self.assertEqual(extra, thing[0:3])

  def test14_loadDefaults(self):
    p = myopt.Parser(extra_args=True)
    p.add_option(short='o', long='option', arg=True, description='', default=0)

    #Provide the absolute path for the test default file, as we do not know
    #the CWD of execution. The assumpition is that the test default file and
    #the this script are in the same directory
    import os.path
    src_dir = os.path.dirname(os.path.realpath(__file__))
    result, extra = p.parse(['-F {}/default.py'.format(src_dir)])
    print(result, extra)
    self.assertEqual(result['option'], 4)

if __name__ == "__main__":
  import sys
  sys.argv.append('-v')
  unittest.main()
  
