#!/usr/bin/env python
# Created by Andre DOS ANJOS <Andre.dos.Anjos@cern.ch>, 19-May-2006

# Defines a python option parser class that is based on getopt.
import copy
import sys
import os.path
import os
import logging 
import importlib.util
import importlib.machinery
import time
import pwd
import getpass

# We can no longer modify local variables with exec() in Python 3
# https://stackoverflow.com/questions/15086040/behavior-of-exec-function-in-python-2-and-python-3

def _coerce_string_(s, ref):
  """Coerces the string (s) given taking the reference value (ref) given."""
  if sys.version_info.major >= 3:
     to_test = (list, dict, tuple, int, float)
  else:
     to_test = (list, dict, tuple, int, float, long)
 
  if type(ref) in to_test:
    retval = eval(s)
  elif ref is not None:
    retval = type(ref)(s)
  else:
    retval = s #keep it a string
  return retval 

def _calculate_list_(l, s):
  """Calculates what to do for appending components to list options."""
  
  if len(s) == 0: 
    # just honour what the user is setting
    l.append(s)
    return l
 
  # if you get to this point, the size of 's' is greater than zero
  if s.strip()[0] == '[': #reset the list
    l = _coerce_string_(s, [])
  else: # the user wants us to append to the existing list
    app = None
    if s.find('.') != -1:
      try: app = float(s)
      except ValueError: pass
    elif s.strip()[-1].lower() == 'l':
      try: app = long(s)
      except ValueError: pass
    else:
      try: app = int(s)
      except ValueError: pass

    if app is None: 
      if s.strip()[0] == '(': app = _coerce_string_(s, ())
      elif s.strip()[0] == '{': app = _coerce_string_(s, {})
      else: app = s
    l.append(app)
    
  return l

class Parser(object): 
  """A OO command line option parser.

  This command line option parser helps you to defined standardized help
  messages and parsing command lines from simple descriptions of options.
  It uses getopt underneath to parse the options themselves.
  """

  def __init__(self, short_intro='', post_options='', extra_args=True):
    """Initializer.

    short_intro and post_options are strings that will be placed before and
    after the options are being printed, to give the user more information
    about the program. Short_intro should be more like a short introduction (a
    few words) on the program, while post_options is intended for usage
    examples or more in-depth, possibly multi-line explanations.
    The value of 'extra_args' should be set to False if extra arguments are
    disallowed for this parser."""
    
    self.short_intro = short_intro
    self.post_options = post_options
    self.extra_args = extra_args
    self.dict = {}
    self.groups = []
    self.default_argument = []
    self.add_option('help', 'h', 'prints this usage message')
    self.add_option('expert-help', '!', 'prints the usage message with all available options', expert=True)
    self.add_option('option-file', 'F', 'loads options from an option file',
                    arg=True, default='')
    self.add_option('dump-options', 'D',
                    'dumps the current default options to stdout',
                    arg=False, default=None)
    self.add_option('dump-expert-options', '@',
                    'dumps all options, including expert options, to stdout',
                    arg=False, default=None, expert=True)

  def add_option(self, long, short, description, arg=False, default=None,
                 group='Global', expert=False):
    """Adds a new option to be parsed.

    Each option must have obligatorily a unique long name. The short name is
    optional. Also optional are default values and description fields. The
    description fields are used for the help printout. If 'arg' is True, this
    means this option is not only a flag, but actually sets a more complex
    value.
    """
    if long in self.dict:
      raise KeyError("Option parser (long) key `%s' already exists" % long)

    if len(long) < 2:
      raise SyntaxError("Long option has to have at least 2 characters (%s)" % long)

    shorts = [self.dict[k]['short'] for k in list(self.dict.keys())]
    if short is not None and len(short) != 0 and short in shorts:
      raise KeyError("Option parser (short) key `%s' already exists" % short)

    if short is not None and len(short) > 1:
      raise SyntaxError("Short option can only have one character (%s)" % short)

    if default is not None and arg is False:
      raise 'Option %s has default (%s) but no argument?' % (long, default)

    self.dict[long] = {'arg': arg,
                       'short': short,
                       'default': copy.deepcopy(default),
                       'description': description,
                       'group': group,
                       'expert': expert}

    if group not in self.groups: self.groups.append(group)

  def usage(self, prefix='usage: %s' % os.path.basename(sys.argv[0]),
      expert=False):
    """Writes a longer help message."""
    retval = ''
    if len(self.short_intro): retval += self.short_intro + '\n'

    # if told to print only non-expert options, remove expert options
    if not expert:
      to_delete = []
      for k, v in self.dict.items(): 
        if v['expert']: to_delete.append(k)
      for k in to_delete: del self.dict[k]

    short_list = [self.dict[k]['short'] for k in list(self.dict.keys()) if
                  (self.dict[k]['short'] is not None and
                   len(self.dict[k]['short']) != 0)]
    
    short_list.sort()
    long_list = list(self.dict.keys())
    long_list.sort()
    shorts = '-[' + ''.join(short_list) + ']'
    longs = '--[' + ','.join(long_list) + ']'
    retval += "%s %s | %s" % (prefix, shorts, longs)
    if self.extra_args: retval += ' [arguments]+'

    self.groups.sort()
    self.groups.remove('Global') #put global options in front
    self.groups.insert(0, 'Global')
    for g in self.groups:
      retval += '\n\n [%s options]\n\n' % g
      start = 0
      for (k,v) in list(self.dict.items()):
        if v['group'] != g: continue
        if v['short'] is not None and len(v['short']) != 0:
          start = max(start, len(' --%s|-%s' % (k, v['short'])))
        else: 
          start = max(start, len(' --%s' % (k)))
      start += 4
      COLUMNS = 78
      remain = COLUMNS - start #what remains for the description column
      for k in long_list:
        if self.dict[k]['group'] != g: continue
        v = self.dict[k]
        explanation = v['description']
        if v['arg'] and v['default'] is not None:
          defval = v['default']
          if type(defval) is str and len(defval) == 0: defval = '<empty>'
          if type(defval) is tuple:
            if len(defval) == 0: defval = '<empty>'
            else: defval = str(defval)
          explanation += ' (defaults to %s)' % defval
        elif v['arg']:
          explanation += " (defaults to `None')"
        elif not v['arg'] and v['default'] is True:
          explanation += " (option locked to True by option file)"
        breaks = (len(explanation)-1) // remain
        if breaks == 0: desc = explanation
        else: desc = ''
        for b in range(0, breaks):
          desc += explanation[(b*remain):((b+1)*remain)]
          desc += '\n' + (' ' * (start+1))
        if breaks != 0: desc += explanation[(breaks*remain):len(explanation)]
        if v['short'] is not None and len(v['short']) != 0:
          line = ' --%s|-%s' % (k, v['short'])
        else:
          line = ' --%s' % (k)
        line = '%s%s%s' % (line, ' '*(start-len(line)), desc)
        retval += line
        
        retval += '\n'
        
    if len(self.post_options): retval += '\n' + self.post_options
    else: retval = retval[0:-1] #remove last two new-lines
    return retval

  def parse(self, argv, prefix='usage: %s' % sys.argv[0]):
    """Parses a command line as given by sys.argv.

    This method will parse the command line given by the first argument and
    will try to coerce type typing information (if defaults are
    available). This means you can pass any python-types and they will be
    covered correctly by this parser. Complex python objects like dictionaries,
    lists and tuples are also treated, but not checked for consistency with
    respect to the defaults (e.g., if the dictionary keys are the same). This
    means something like --mydict="{'x': 'hello', 'y': 3.14}" will be correctly
    treated and typed, but only if a default is given for the option with the
    same name."""

    from getopt import gnu_getopt
    from sys import exit
    
    shorts = ''
    longs = []
    for (k,v) in list(self.dict.items()):
      if v['short'] is not None: shorts += v['short']
      to_append = k
      if v['arg']:
        if v['short']: shorts += ':'
        to_append += '='
      longs.append(to_append)
    optlist, args = gnu_getopt(argv, shorts, longs)

    if (len(args)) and (self.extra_args is False):
      raise SyntaxError("Parser doesn't accept extra arguments => %s" % \
            ' '.join(args))

    retval = {} # a dictionary of things that were set
    for (o,a) in optlist:
      if o in ('-h', '--help', '-?', '--usage'):
        print(self.usage(prefix))
        exit(0)
      if o in ('--expert-help', '-!'):
        print(self.usage(prefix, expert=True))
        exit(0)
      if o in ('-F', '--option-file'):
        # this will make your defaults (if any) change
        self.reload_defaults(a) #changes self.dict!!
        if self.default_argument and not args: args = self.default_argument
      if o in ('-D', '--dump-options'):
        # this will make your defaults (if any) change
        print(self.dump_defaults(retval, extra_args=args))
        exit(0)
      if o in ('-@', '--dump-expert-options'):
        # this will make your defaults (if any) change
        print(self.dump_defaults(retval, extra_args=args, expert=True))
        exit(0)
      else:
        for (k,v) in list(self.dict.items()):
          if k in ('usage', 'help', 'option-file', 'dump-options'): continue
          if o == ('--%s' % k) or o == ('-%s' % v['short']):
            if not v['arg']: retval[k] = True #it is a flag
            else: 
              if type(v['default']) is list:
                if k not in retval: retval[k] = v['default']
                retval[k] = _calculate_list_(retval[k], a)
              else:
                retval[k] = _coerce_string_(a, v['default'])

                # if the user resets the option to be of list type, change the
                # default so we start accepting multiple arguments
                if isinstance(retval[k], list): v['default'] = []
              
    for (k,v) in list(self.dict.items()):
      if k not in list(retval.keys()): #check
        if k in ('usage', 'help', 'option-file', 'dump-options'): continue
        if not v['arg'] and v['default'] is None: retval[k] = False
        elif not v['arg'] and v['default'] is not None: retval[k] = v['default']
        else: retval[k] = v['default']

    if self.extra_args: return (retval, args)
    return retval
    
  def reload_defaults(self, f):
    """Reloads the defaults from the given python file f.

    This method will reload all the default values available in the python
    module given in the input. This file has a free (as in python) format. It
    can contain advanced instructions or just variable assignments. This method
    will work by searching a dictionary with name 'option' and comparing its
    keys to the keys available for the current module, switching the default on
    those options to the new assigned values. The importing happens by normal
    python means. """
    
    #we first search at the current working directory, then the 
    #directory at the runner distribution and by last at the PYTHONPATH
    to_import = os.path.basename(f).replace('.py','')
    paths = [os.getcwd()]
    add_dir = os.path.realpath(os.path.dirname(f.lstrip()))
    if len(add_dir) != 0: paths.insert(0, add_dir)
    paths.extend(sys.path)
    default_specs = importlib.machinery.PathFinder.find_spec(to_import, paths)

    if default_specs is None:
      logging.error('Cannot import option file `%s`' % f)
      raise ImportError('Cannot import option file `%s`' % f)

    logging.info('Loading options "%s" from "%s"' % (to_import, default_specs.origin))

    pmmod = importlib.util.module_from_spec(default_specs)
    default_specs.loader.exec_module(pmmod)

    presets = pmmod.__dict__.get('option', {})
    for k, v in list(presets.items()):
      if k in self.dict:
        self.dict[k]['default'] = v
      else:
        logging.warning('User option "%s" ignored. Not available at program catalog.' % (k))

    user_arguments = pmmod.__dict__.get('arguments', [])
    if self.extra_args:
      self.default_argument = user_arguments
    elif user_arguments:
      for k in user_arguments:
        logging.warning('Ignoring extra argument "%s". This parser does not support them.' % k)


  def dump_defaults(self, current={}, extra_args=[], expert=False):
    """Dumps the defaults to standard output.

    This method will dump the (current) defaults and option names to the
    standard output so it is easy to debug what are the current defaults and
    encode new option files. The format is already the one which accepted
    back.
    """

    retval = ''
    retval += "# -- CUT FROM THIS POINT UPWARDS --\n"
    retval += "# This dump was automatically generated by myopt\n"
    retval += "# User %s (%s) at %s\n\n" % (getpass.getuser(),
                                            pwd.getpwnam(getpass.getuser())[4],
                                            time.asctime())
    retval += "# All options should be inside the 'option' dictionary\n"
    retval += "# The keys being the long name of the options and the\n"
    retval += "# values, the new default to be attributed.\n"
    retval += "option = {}\n\n"
    groups = {}
    
    # if told to print only non-expert options, remove expert options
    if not expert:
      to_delete = []
      for k, v in self.dict.items(): 
        if v['expert']: to_delete.append(k)
      for k in to_delete: del self.dict[k]

    for k, v in self.dict.items():
      if k in current: self.dict[k]['default'] = current[k] 
      x = groups.get(v['group'], {})
      x[k] = v
      groups[v['group']] = x
    sorted_keys = list(groups.keys())
    sorted_keys.sort()
    for g in sorted_keys:

      #first we sort out if there are emtpy groups, we don't print those!
      for k, v in list(groups[g].items()):
        if k in ('usage', 'help', 'option-file', 'dump-options', 'expert-help',
            'dump-expert-options'):
          del groups[g][k]
      if len(list(groups[g].items())) == 0: 
        continue

      #for the remaining, we print
      retval += "# ================================\n"
      retval += "# '%s' options \n" % g
      retval += "# ================================\n\n"
      for k, v in list(groups[g].items()):
        retval += "# %s\n" % (v["description"])
        if v["expert"]: retval += "# -- expert option --\n"
        if v["arg"]:
          if type(v["default"]) == str:
            retval += "option['%s'] = '%s'" % (k, v["default"]) 
          else:
            retval += "option['%s'] = %s" % (k, v["default"])
        elif not v["arg"] and v["default"] == True:
          retval += "option['%s'] = True" % k
        else:
          retval += "option['%s'] = False" % k
        retval += '\n\n'

    #the extra arguments
    if extra_args:
      retval += "# ================================\n"
      retval += "# Extra arguments \n"
      retval += "# ================================\n\n"
      retval += "arguments = []\n\n"
      for k in extra_args:
        retval += '# argument[%d]\n' % extra_args.index(k)
        retval += "arguments.append('%s')\n\n" % k

    retval = retval[0:-1] #remove last new-line
    return retval
